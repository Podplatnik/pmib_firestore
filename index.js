const admin = require('firebase-admin');
const functions = require('firebase-functions');
const fillCustomers = require('./app/customer');
const fillSources = require('./app/source');
const fillPaymentIntents = require('./app/paymentIntent');
const serviceAccount = require("./config/jusnik-pmib-key.json");
const queryHandler = require('./app/queryHandler');




admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://jusnik-pmib.firebaseio.com"
});

const db = admin.firestore();


async function app(){
    const customers = await fillCustomers(db);

    const sources = await fillSources(db, customers);
    await fillPaymentIntents(db, customers, sources)
}


// app()

async function query(db) {
    await queryHandler(db);
}
query(db);