const CustomerBuilder = require('./models/CustomerBuilder');
const randomstring = require("randomstring");
const randomInt = require('random-int');
const randomEmail = require('random-email');
const { Random } = require("random-js");
const random = new Random();

module.exports = async function fillCustomers(db) {


    const customerDocumentIds = [];
    for (let i = 0; i < 20; i++) {
        const currencyOptions = ['eur', 'usd', 'hrk', 'gbp', 'jpy', 'dkk'];
        const customerStripeId = `cus_${randomstring.generate(14)}`;
        const documentId = db.collection("customers").doc().id;
        const restaurantId = i + 1;
        const restaurantEmail = randomEmail();
        const numberOfCurrencies = random.integer(1, 6);
        const pickedCurrencies = [];
        for (let i = 0; i < numberOfCurrencies; i++){
            pickedCurrencies.push(currencyOptions[i])
        }

        const randomDate = random.date(new Date(2016, 1, 1), new Date());
        const randomTimestamp = Math.round(randomDate.getTime() / 1000);

        const customer = new CustomerBuilder()
            .setBalance(randomInt(10000))
            .setCreated(randomTimestamp)
            .setCurrencies(pickedCurrencies)
            .setDefaultSource(null)
            .setDescription(`Customer for restaurant ${randomstring.generate(7)}`)
            .setEmail(restaurantEmail)
            .setInvoicePrefix(true)
            .setInvoiceSettings({
                customer_fields: null,
                default_payment_method: null,
                footer: null
            })
            .setMetadata({
                restaurant_id: restaurantId
            })
            .setObject('customer')
            .setStripeId(customerStripeId);

        const customerRef = db.collection('customers').doc(documentId);
        const customerJson = JSON.parse(JSON.stringify(customer));

        const setCustomer = await customerRef.set(customerJson);
        customerDocumentIds.push({
            customerStripeId,
            documentId,
            restaurantId,
            restaurantEmail
        })
    }
    return customerDocumentIds
};
