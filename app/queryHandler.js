function displayQuery(query, name) {
    console.log('****************************************************');
    console.log(`** START -DISPLAYING QUERY: ${name}`);
    query
        .then(snapshot => {
            if (snapshot.empty) {
                console.log('No matching documents.');
                return;
            }

            snapshot.forEach(doc => {
                console.log(doc.id, '=>', doc.data());
            });
        })
        .catch(err => {
            console.log('Error getting documents', err);
        })
        .finally(() => {
            console.log(`** END -DISPLAYING QUERY: ${name}\n\r`);
        })
}

module.exports = async function runQueries(db) {
    // 1) Pridobi nazadnje dodan račun
    // displayQuery(db.collection('invoices').orderBy('created', 'desc').limit(1).get())

    // 2) Pridobi število računov, ki niso v celoti plačani
    // db.collection('invoices').where('amount_remaining', '>', 0).get().then(snapshot => {
    //     console.log('Število računov, ki niso plačani v celoti: ', snapshot.size)
    // })

    // 3) Pridobi najmanjše račune od 4. do 8. mesta
    // displayQuery(db.collection('invoices').orderBy('amount_paid', 'asc').startAfter(4).limit(4).get())

    // 4) Pridobi število vseh plačilna sredstva na produkcijskem okolju
    // db.collection('sources').where('livemode', '==', true).get().then(snapshot => {
    //     console.log('Število računov na produkcijskem okolju: ', snapshot.size)
    // })

    // 5) Pridobi število vseh plačilna sredstva na testnem okolju okolju, ki so bila narejena prej kot v 2019
    // db.collection('sources').where('livemode', '==', false).where('created', '<', 1514764800).get().then(snapshot => {
    //     console.log('Število računov na test okolju pred 2018: ', snapshot.size)
    // })

    // 6) Pridobi vsa plačila, ki majo davek
    // displayQuery(db.collection('paymentIntents').where('application_fee_amount', '>=', 1).get())

    // 7) Pridobi vse uporabnike, ki lahko plačujejo z gbp
    // displayQuery(db.collection('customers').where('currencies', 'array-contains', 'dkk').get())

    // 8) Pridobi število plačilnih sredstev, ki so bila trajno preklicana, neuspela
    // db.collection('sources').where('status', 'in', ['canceled', 'failed']).get().then(snapshot => {
    //     console.log('Število plačilnih sredstev, ki so bila trajno preklica,a neuspela: ', snapshot.size)
    // })

    // 9) Pridobi vse račune, ki so bili avtomatsko plačani z dolarji z davkom 3%
    // displayQuery(db.collection('invoices')
    //     .where('collection_method', '==', 'automatic')
    //     .where('currency', '==', 'eur')
    //     .where('paid', '==', true)
    //     .where('application_fee_amount', '==', 3).get())

    // 10) Pridobi uporabnike, ki majo na računu več kot 7000 enot denarja
    // displayQuery(db.collection('customers').where('balance', '>', 7000).get())


    // 11) Pridobi število vsa plačila za najstarejšo restavracijo
    // db.collection('customers').orderBy('created', 'desc').limit(1).get()
    //     .then(snapshot => {
    //         snapshot.forEach(doc => {
    //             db.collection('paymentIntents').where('customer', '==', doc.data().stripe_id).get()
    //                 .then(secondSnapshot => {
    //                     console.log('Število vseh plačil za najstarejšega uporabnika: ', secondSnapshot.size)
    //                 })
    //         });
    //     })


    // 12) Pridobi število računov, ki niso plačani v evrih
    // db.collection('invoices').where('currency', 'in', ['usd', 'hrk', 'gbp']).get().then(snapshot => {
    //     console.log('Število računov, ki niso plačani v evrih: ', snapshot.size)
    // })

    // 13) Pridobi vsa plačila, ki potrebujejo potrditev uporabnika
    // db.collection('paymentIntents').where('status', '==', 'requires_confirmation').get().then(snapshot => {
    //     console.log('Plačila, ki potrebujejo potrditev uporabnikah: ', snapshot.size)
    // })

    // 14) Pridobi vse neobdavčene račune nad 2000 enot
    // db.collection('invoices').where('application_fee_amount', '==', 0).where('amount_paid', '>', 2000).get().then(snapshot => {
    //     console.log('Vse neobdavčene račune nad 2000 enot: ', snapshot.size)
    // })

    // 15) Pridobi vsa, neobdavčena, plačila v kunah, ki so bila uspešno zaključena na produkciji
    displayQuery(db.collection('paymentIntents')
        .where('currency', '==', 'hrk')
        .where('application_fee_amount', '==', 0)
        .where('livemode', '==', true)
        .where('status', '==', 'succeeded')
        // .where('created', '<', 1546300800)
        // .orderBy('created', 'desc').limit(1)
        .get())
};