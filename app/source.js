const SourceBuilder = require('./models/SourceBuilder');
const randomstring = require("randomstring");
const randomInt = require('random-int');
const randomEmail = require('random-email');
const { Random } = require("random-js");
const random = new Random();


module.exports = async function fillSources(db, customers) {
    const sources = [];
    for (let i = 0; i < 20; i++){
        const sourceStripeId = `src_${randomstring.generate(24)}`;
        const documentId = db.collection("sources").doc().id;
        const randomDate = random.date(new Date(2016, 1, 1), new Date());
        const randomTimestamp = Math.round(randomDate.getTime() / 1000);
        const statusArray = ['canceled', 'chargeable', 'consumed', 'failed', 'pending'];
        const livemodeBool = random.bool();
        const source = new SourceBuilder()
            .setStripeId(sourceStripeId)
            .setObject('source')
            .setCreated(randomTimestamp)
            .setCurrency('eur')
            .setLivemode(livemodeBool)
            .setCustomer(customers[i].customerStripeId)
            .setOwner({
                "address": null,
                "email": customers[i].restaurantEmail,
                "name": null,
                "phone": null,
                "verified_address": null,
                "verified_email": null,
                "verified_name": null,
                "verified_phone": null
            })
            .setUsage( (random.bool()) ? 'reusable' : 'single_use')
            .setStatus(random.pick(statusArray));

        const sourceRef = db.collection('sources').doc(documentId);
        const sourceJson = JSON.parse(JSON.stringify(source));
        const setSource = await sourceRef.set(sourceJson);
        sources.push({
            customerStripeId: customers[i].customerStripeId,
            sourceStripeId,
            livemodeBool
        });
    }
    return sources;
};