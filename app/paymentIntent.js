const PaymentIntentBuilder = require('./models/PaymentIntentBuilder');
const InvoiceReceiptBuilder = require('./models/InvoiceReceiptBuilder');
const { Random } = require("random-js");
const random = new Random();
const randomstring = require("randomstring");

module.exports = async function fillPaymentIntents(db, customers, sources) {
    const paymentIntents = [];
    const currencyOptions = ['eur', 'usd', 'hrk', 'gbp'];
    const invoiceStatusOptions = ['draft', 'paid', 'open', 'uncollectible', 'void'];
    const captureMethodOptions = ['automatic', 'manual'];
    for (let i = 0; i < 20; i++) {
        const targetedSource = sources.find(element => element.customerStripeId = customers[i].customerStripeId);
        const randomNumberOfIntents = random.integer(2, 24);
        for (let j = 0; j < randomNumberOfIntents; j++){
            const invoiceStripeId = `in_${randomstring.generate(24)}`;
            const paymentIntentStripeId = `pi_${randomstring.generate(24)}`;
            const documentId = db.collection("sources").doc().id;
            const randomDate = random.date(new Date(2016, 1, 1), new Date());
            const randomTimestamp = Math.round(randomDate.getTime() / 1000);
            const pickedCurrency = random.pick(currencyOptions);
            const amount = random.integer(500, 3000);
            const applicationFeeAmount = random.integer(0, 3);
            const paid = random.bool(0.7);
            const amountPaid = (paid) ? amount : amount - random.integer(1, amount);
            const pickedMethod = random.pick(captureMethodOptions);
            const statusOptions = ['requires_payment_method', 'requires_confirmation', 'requires_action', 'processing', 'requires_capture', 'canceled', 'succeeded'];
            const paymentIntent = new PaymentIntentBuilder()
                .setStripeId(paymentIntentStripeId)
                .setObject('payment_intent')
                .setCreated(randomTimestamp)
                .setCurrency(pickedCurrency)
                .setAmount(amount)
                .setInvoice(invoiceStripeId)
                .setAmountCapturable(amount)
                .setAmountReceived(amountPaid)
                .setApplicationFeeAmount(applicationFeeAmount)
                .setCanceledAt((random.bool(0.9)) ? null : randomTimestamp + 10)
                .setCaptureMethod(pickedMethod)
                .setCustomer(customers[i].customerStripeId)
                .setReceiptEmail(customers[i].restaurantEmail)
                .setStatus(random.pick(statusOptions))
                .setLivemode(true);

            const paymentIntentRef = db.collection('paymentIntents').doc(documentId);
            const paymentIntentJson = JSON.parse(JSON.stringify(paymentIntent));

            const setPaymentIntent = await paymentIntentRef.set(paymentIntentJson);



            const invoiceDocumentId = db.collection("invoices").doc().id;
            const receiptNumberFirstPart = randomstring.generate({
                length: 4,
                charset: 'numeric'
            });
            const receiptNumberSecondPart = randomstring.generate({
                length: 4,
                charset: 'numeric'
            });
            const invoice = new InvoiceReceiptBuilder()
                .setStripeId(invoiceStripeId)
                .setObject('invoice')
                .setCreated(randomTimestamp)
                .setCurrency(pickedCurrency)
                .setPaymentIntent(paymentIntentStripeId)
                .setAmountDue(amount)
                .setAmountPaid(amountPaid)
                .setAmountRemaining(amount - amountPaid)
                .setApplicationFeeAmount(applicationFeeAmount)
                .setCollectionMethod(pickedMethod)
                .setCustomer(customers[i].customerStripeId)
                .setCustomerEmail(customers[i].restaurantEmail)
                .setPaid(paid)
                .setReceiptNumber(`${receiptNumberFirstPart}-${receiptNumberSecondPart}`)
                .setTotal(amount)
                .setDefaultSource(targetedSource.sourceStripeId)
                .setStatus(random.pick(invoiceStatusOptions))
                .setItems(InvoiceReceiptBuilder.generateItems(random.integer(1, 5), amountPaid, pickedCurrency));
            const invoiceRef = db.collection('invoices').doc(invoiceDocumentId);
            const invoiceJson = JSON.parse(JSON.stringify(invoice));
            const setInvoice = await invoiceRef.set(invoiceJson);
        }
    }
};