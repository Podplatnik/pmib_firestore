class CustomerBuilder {
    constructor(){
        this.balance = undefined;
        this.created = undefined;
        this.currencies = undefined;
        this.default_source = undefined;
        this.description = undefined;
        this.email = undefined;
        this.invoice_prefix = undefined;
        this.invoice_settings = undefined;
        this.object = undefined;
        this.stripe_id = undefined;
        this.metadata = undefined;
    }

    setBalance(balance) {
        this.balance = balance;
        return this;
    }

    setCreated(created) {
        this.created = created;
        return this;
    }

    setCurrencies(currencies) {
        this.currencies = currencies;
        return this;
    }

    setDefaultSource(default_source) {
        this.default_source = default_source;
        return this;
    }

    setDescription(description) {
        this.description = description;
        return this;
    }

    setEmail(email){
        this.email = email;
        return this;
    }

    setInvoicePrefix(invoice_prefix) {
        this.invoice_prefix = invoice_prefix;
        return this;
    }

    setInvoiceSettings(invoice_settings) {
        this.invoice_settings = invoice_settings;
        return this;
    }

    setObject(object) {
        this.object = object;
        return this;
    }

    setStripeId(stripe_id) {
        this.stripe_id = stripe_id;
        return this;
    }

    setMetadata(metadata) {
        this.metadata = metadata;
        return this;
    }

}

module.exports = CustomerBuilder;