class PaymentIntentBuilder {
    constructor() {
        this.stripe_id = undefined;
        this.object = undefined;
        this.created = undefined;
        this.currency = undefined;
        this.amount = undefined;
        this.amount_capturable = undefined;
        this.amount_received = undefined;
        this.application_fee_amount = undefined;
        this.canceled_at = undefined;
        this.capture_method = undefined;
        this.customer = undefined;
        this.livemode = undefined;
        this.receipt_email = undefined;
        this.status = undefined;
        this.invoice = undefined;
    }

    setStripeId(stripe_id) {
        this.stripe_id = stripe_id;
        return this;
    }

    setInvoice(invoice) {
        this.invoice = invoice;
        return this;
    }

    setObject(object) {
        this.object = object;
        return this;
    }

    setCreated(created) {
        this.created = created;
        return this;
    }

    setCurrency(currency) {
        this.currency = currency;
        return this;
    }

    setAmount(amount) {
        this.amount = amount;
        return this;
    }

    setAmountCapturable(amount_capturable) {
        this.amount_capturable = amount_capturable;
        return this;
    }

    setApplicationFeeAmount(application_fee_amount) {
        this.application_fee_amount = application_fee_amount;
        return this;
    }

    setAmountReceived(amount_received) {
        this.amount_received = amount_received;
        return this;
    }

    setCanceledAt(canceled_at) {
        this.canceled_at = canceled_at;
        return this;
    }

    setCaptureMethod(capture_method) {
        this.capture_method = capture_method;
        return this;
    }

    setCustomer(customer) {
        this.customer = customer;
        return this;
    }

    setLivemode(livemode) {
        this.livemode = livemode;
        return this;
    }

    setReceiptEmail(receipt_email) {
        this.receipt_email = receipt_email;
        return this;
    }

    setStatus(status){
        this.status = status;
        return this;
    }

}

module.exports = PaymentIntentBuilder;