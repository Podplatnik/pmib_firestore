const { Random } = require("random-js");
const random = new Random();
const randomstring = require("randomstring");

class InvoiceReceiptBuilder {
    constructor() {
        this.stripe_id = undefined;
        this.object = undefined;
        this.created = undefined;
        this.currency = undefined;
        this.payment_intent = undefined;
        this.amount_due = undefined;
        this.amount_paid = undefined;
        this.amount_remaining = undefined;
        this.application_fee_amount = undefined;
        this.collection_method = undefined;
        this.customer = undefined;
        this.customer_email = undefined;
        this.default_source = undefined;
        this.paid = undefined;
        this.receipt_number = undefined;
        this.status = undefined;
        this.total = undefined;
        this.items = undefined;
    }

    setStripeId(stripe_id) {
        this.stripe_id = stripe_id;
        return this;
    }

    setObject(object) {
        this.object = object;
        return this;
    }

    setCreated(created) {
        this.created = created;
        return this;
    }

    setCurrency(currency) {
        this.currency = currency;
        return this;
    }

    setPaymentIntent(payment_intent) {
        this.payment_intent = payment_intent;
        return this;
    }

    setAmountDue(amount_due) {
        this.amount_due = amount_due;
        return this;
    }

    setAmountPaid(amount_paid) {
        this.amount_paid = amount_paid;
        return this;
    }

    setAmountRemaining(amount_remaining) {
        this.amount_remaining = amount_remaining;
        return this;
    }

    setApplicationFeeAmount(application_fee_amount) {
        this.application_fee_amount = application_fee_amount;
        return this;
    }

    setCollectionMethod(collection_method) {
        this.collection_method = collection_method;
        return this;
    }

    setCustomer(customer) {
        this.customer = customer;
        return this;
    }

    setCustomerEmail(customer_email) {
        this.customer_email = customer_email;
        return this;
    }

    setDefaultSource(default_source) {
        this.default_source = default_source;
        return this;
    }

    setPaid(paid) {
        this.paid = paid;
        return this;
    }

    setReceiptNumber(receipt_number) {
        this.receipt_number = receipt_number;
        return this;
    }

    setTotal(total) {
        this.total = total;
        return this;
    }

    setStatus(status) {
        this.status = status;
        return this;
    }

    setItems(items) {
        this.items = items;
        return this;
    }

    static generateItems(numberOfItems, amount, currency){
        let amountRemaining = amount;
        const items = [];
        for (let i = 0; i < numberOfItems; i++){
            const last = (i + 1 === numberOfItems);
            const itemCost = (last) ? amountRemaining : amountRemaining - random.integer(0, amountRemaining);
            items.push({
                id: `it_${randomstring.generate(12)}`,
                currency,
                description: randomstring.generate(20),
                amount: itemCost
            });
            amountRemaining = amountRemaining - itemCost;
        }
        return items;
    }

}

module.exports = InvoiceReceiptBuilder;