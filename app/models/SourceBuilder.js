class SourceBuilder{
    constructor() {
        this.stripe_id = undefined;
        this.object = undefined;
        this.created = undefined;
        this.currency = undefined;
        this.livemode = undefined;
        this.customer = undefined;
        this.owner = undefined;
        this.usage = undefined;
        this.status = undefined;
    }

    setStripeId(stripe_id) {
        this.stripe_id = stripe_id;
        return this;
    }

    setObject(object) {
        this.object = object;
        return this;
    }

    setCreated(created) {
        this.created = created;
        return this;
    }

    setCurrency(currency) {
        this.currency = currency;
        return this;
    }

    setLivemode(livemode) {
        this.livemode = livemode;
        return this;
    }

    setCustomer(customer) {
        this.customer = customer;
        return this;
    }

    setOwner(owner){
        this.owner = owner;
        return this;
    }

    setUsage(usage) {
        this.usage = usage;
        return this;
    }

    setStatus(status) {
        this.status = status;
        return this;
    }
}

module.exports = SourceBuilder;